package com.mobilehub.svernekar.myapplication.ui;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mobilehub.svernekar.myapplication.R;
import com.mobilehub.svernekar.myapplication.model.example.Result;

/**
 * Created by svernekar003 on 19/12/17.
 */

public class ItemView extends ConstraintLayout {
    public ItemView(Context context) {
        super(context);
    }

    public ItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setResult(Result result) {
        TextView trackName = findViewById(R.id.track_name);
        TextView name = findViewById(R.id.artist_name);
        ImageView imageView = findViewById(R.id.artist_image);
        Glide.with(getContext())
                .load(result.getArtworkUrl100())

                .into(imageView);
        name.setText(result.getArtistName());
        trackName.setText(result.getTrackName());
    }
}
