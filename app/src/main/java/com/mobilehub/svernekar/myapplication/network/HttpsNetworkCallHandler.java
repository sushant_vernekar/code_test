package com.mobilehub.svernekar.myapplication.network;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;

import com.mobilehub.svernekar.myapplication.model.example.Example;
import com.mobilehub.svernekar.myapplication.presenter.IPresenterCallback;

import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.mobilehub.svernekar.myapplication.network.Apis;

/**
 * Created by svernekar003 on 18/12/17.
 */

public class HttpsNetworkCallHandler {
    private Retrofit retrofit;
    private Example example;


    public HttpsNetworkCallHandler() {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl("https://itunes.apple.com");
        builder.addConverterFactory(GsonConverterFactory.create());
        retrofit = builder.build();
    }

    public void makeCall(String searchTerms, final IPresenterCallback iPresenterCallback) {

        IGetTracks iGetTracks = retrofit.create(IGetTracks.class);
        final Call<Example> exampleCall = iGetTracks.getTracks(searchTerms);
        exampleCall.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                example = response.body();
                iPresenterCallback.onSuccess(example);

            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                iPresenterCallback.onError();
            }
        });

    };

}
