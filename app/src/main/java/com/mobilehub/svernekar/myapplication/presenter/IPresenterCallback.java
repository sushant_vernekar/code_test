package com.mobilehub.svernekar.myapplication.presenter;

import com.mobilehub.svernekar.myapplication.model.example.Example;

/**
 * Created by svernekar003 on 18/12/17.
 */

public interface IPresenterCallback {

    public void onSuccess(Example example);

    public void onError();

}
