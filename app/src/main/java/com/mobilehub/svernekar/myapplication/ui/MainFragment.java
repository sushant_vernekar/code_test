package com.mobilehub.svernekar.myapplication.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.mobilehub.svernekar.myapplication.model.example.Example;
import com.mobilehub.svernekar.myapplication.model.example.Result;
import com.mobilehub.svernekar.myapplication.network.HttpsNetworkCallHandler;
import com.mobilehub.svernekar.myapplication.presenter.IPresenterCallback;

import java.util.List;

/**
 * Created by svernekar003 on 18/12/17.
 */

public class MainFragment extends ListFragment {
    private ArtistsAdapter artistsAdapter;
    private List<Result> results;


    public void onArtistChanged(String artist) {
        loadArtists(artist);
    }


    @Override
    public void onResume() {
        super.onResume();
        artistsAdapter = new ArtistsAdapter(getContext(), null);
        setListShown(true);
    }

    private void loadArtists(String artist) {
        setListShown(false);
        HttpsNetworkCallHandler httpsNetworkCallHandler = new HttpsNetworkCallHandler();
        httpsNetworkCallHandler.makeCall(artist, new IPresenterCallback() {
            @Override
            public void onSuccess(Example example) {
                Log.d("MainFragment", example.toString());
                results = example.getResults();
                artistsAdapter.setResults(results);
                setListAdapter(artistsAdapter);
                artistsAdapter.notifyDataSetChanged();
                setListShown(true);
            }

            @Override
            public void onError() {
                setListShown(true);
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        startActivity(new Intent(getContext(),DetailsActivity.class).putExtra("result", results.get(position)));
    }
}
