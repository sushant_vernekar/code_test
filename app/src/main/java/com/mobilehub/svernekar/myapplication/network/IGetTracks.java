package com.mobilehub.svernekar.myapplication.network;

import com.mobilehub.svernekar.myapplication.model.example.Example;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by svernekar003 on 18/12/17.
 */

public interface IGetTracks {

    @GET("/search")
    Call<Example> getTracks(@Query("term") String terms);
}
