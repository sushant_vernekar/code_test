package com.mobilehub.svernekar.myapplication.ui;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mobilehub.svernekar.myapplication.R;
import com.mobilehub.svernekar.myapplication.model.example.Result;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DetailsActivity extends AppCompatActivity {
    private AppCompatTextView name;
    private AppCompatTextView album_name;
    private AppCompatTextView release_date;
    private AppCompatTextView price;
    private AppCompatTextView track_name;
    private ImageView artwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);
        Result result = getIntent().getParcelableExtra("result");
        name = findViewById(R.id.artist_name);
        album_name = findViewById(R.id.artist_album_name);
        release_date = findViewById(R.id.release_date);
        price = findViewById(R.id.price);
        track_name = findViewById(R.id.track_name);
        artwork = findViewById(R.id.artist_image);

        name.setText("Name :" + result.getArtistName());
        album_name.setText("Album Name :" + result.getTrackCensoredName());
        price.setText("Price :" + String.valueOf(result.getTrackPrice()) + " USD");
        release_date.setText("Release date :" + formatTimeUTC(result.getReleaseDate()));
        track_name.setText("Track Name :" + result.getTrackName());
        Glide.with(this)
                .load(result.getArtworkUrl100())

                .into(artwork);
    }

    private String formatTimeUTC(String date) {
        Date sourceDate1 = new Date();
        TimeZone localTimeZone = TimeZone.getTimeZone(TimeZone.getDefault().getID());
        TimeZone serverTimeZone = TimeZone.getTimeZone("UTC");
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        serverFormat.setTimeZone(serverTimeZone);
        SimpleDateFormat localFormat = new SimpleDateFormat("MM/dd/yyyy");
        localFormat.setTimeZone(localTimeZone);

        if (date.contains("T")) {
            try {
                sourceDate1 = serverFormat.parse(date);

            } catch (ParseException e) {
                Log.d("ParseException", e.getLocalizedMessage(), e);
            }
        } else {
            try {
                sourceDate1 = localFormat.parse(date);
            } catch (ParseException e) {
                Log.e("ParseException", e.getLocalizedMessage(), e);
            }
        }
        return localFormat.format(sourceDate1);
    }
}
