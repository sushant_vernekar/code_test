package com.mobilehub.svernekar.myapplication.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mobilehub.svernekar.myapplication.R;
import com.mobilehub.svernekar.myapplication.model.example.Example;
import com.mobilehub.svernekar.myapplication.model.example.Result;

import java.util.List;

/**
 * Created by svernekar003 on 19/12/17.
 */

public class ArtistsAdapter extends BaseAdapter {
    private Context context;

    public void setResults(List<Result> results) {
        this.results = results;
    }

    private List<Result> results;

    public ArtistsAdapter(Context context, Example example) {
        this.context = context;

    }

    @Override
    public int getCount() {
        if (results == null)

            return 0;
        else return results.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ItemView itemView;
        if (view == null)
            itemView = (ItemView) LayoutInflater.from(context).inflate(R.layout.item_view, null, false);
        else
            itemView = (ItemView) view;

        itemView.setResult(results.get(i));
        return itemView;
    }
}
