
package com.mobilehub.svernekar.myapplication.model.example;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Result implements Parcelable {


    @SerializedName("artistName")
    @Expose
    private String artistName;
    @SerializedName("collectionName")
    @Expose
    private String collectionName;
    @SerializedName("trackName")
    @Expose
    private String trackName;

    @SerializedName("artworkUrl100")
    @Expose
    private String artworkUrl100;

    public String getTrackCensoredName() {
        return trackCensoredName == null ? "" :trackCensoredName;
    }

    public void setTrackCensoredName(String trackCensoredName) {
        this.trackCensoredName = trackCensoredName;
    }

    @SerializedName("trackCensoredName")
    @Expose
    private String trackCensoredName;
    @SerializedName("trackPrice")
    @Expose
    private float trackPrice;
    @SerializedName("releaseDate")
    @Expose
    private String releaseDate;


    public final static Creator<Result> CREATOR = new Creator<Result>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        public Result[] newArray(int size) {
            return (new Result[size]);
        }

    };

    protected Result(Parcel in) {
        this.artistName = ((String) in.readValue((String.class.getClassLoader())));
        this.collectionName = ((String) in.readValue((String.class.getClassLoader())));
        this.trackName = ((String) in.readValue((String.class.getClassLoader())));
        this.artworkUrl100 = ((String) in.readValue((String.class.getClassLoader())));
        this.trackPrice = ((float) in.readValue((float.class.getClassLoader())));
        this.releaseDate = ((String) in.readValue((String.class.getClassLoader())));
        this.trackCensoredName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Result() {
    }


    public String getArtistName() {
        return artistName == null ? "" :artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getCollectionName() {
        return collectionName == null ? "" :collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public String getTrackName() {
        return trackName == null ? "" :trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }


    public String getArtworkUrl100() {
        return artworkUrl100;
    }

    public void setArtworkUrl30(String artworkUrl60) {
        this.artworkUrl100 = artworkUrl60;
    }


    public float getTrackPrice() {
        return  trackPrice == 0f ? 0 :trackPrice;
    }

    public void setTrackPrice(float trackPrice) {
        this.trackPrice = trackPrice;
    }

    public String getReleaseDate() {
        return releaseDate == null ? "" :releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(artistName);
        dest.writeValue(collectionName);
        dest.writeValue(trackName);
        dest.writeValue(artworkUrl100);
        dest.writeValue(trackPrice);
        dest.writeValue(releaseDate);
        dest.writeValue(trackCensoredName);
    }

    public int describeContents() {
        return 0;
    }

}
